<%-- 
    Document   : listePersonne
    Created on : 18-Nov-2022, 15:38:38
    Author     : SOATIANA
--%>

<%@page import="models.Personne"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String nom = (String) request.getAttribute("nom");
    String pnom = (String) request.getAttribute("prenom");
    int age = (int) request.getAttribute("age");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Infos sur la personne</h1>
        <p><b>Nom: </b><%= nom%></p>
        <p><b>Prenom: </b><%= pnom%></p>
        <p><b>Age: </b><%= age %></p>
        <br/>
        <a href="goToForm.do">Retour a l'insertion</a>
    </body>
</html>

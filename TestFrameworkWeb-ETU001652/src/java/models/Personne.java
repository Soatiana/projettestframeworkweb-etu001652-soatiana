/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import annotations.MyAnnotation;
import java.util.ArrayList;
import java.util.HashMap;
import modelview.ModelView;

/**
 *
 * @author SOATIANA
 */
public class Personne {
    private String nomPersonne;
    private String prenomPersonne;
    private int agePersonne;

    public String getNomPersonne() {
        return nomPersonne;
    }

    public void setNomPersonne(String nomPersonne) {
        this.nomPersonne = nomPersonne;
    }

    public String getPrenomPersonne() {
        return prenomPersonne;
    }

    public void setPrenomPersonne(String prenomPersonne) {
        this.prenomPersonne = prenomPersonne;
    }

    public int getAgePersonne() {
        return agePersonne;
    }

    public void setAgePersonne(int agePersonne) {
        this.agePersonne = agePersonne;
    }
    
//    Redirection vers insertPersonne.jsp
    @MyAnnotation(url="goToForm.do")
    public ModelView generateForm(){
        ModelView valiny = new ModelView();
        valiny.setJspToGo("insertPersonne.jsp");
        return valiny;
    }
    
//    Avoir une personne (pour ensuite afficher les informations sur cette personne)
    @MyAnnotation(url="insertThis.do")
    public ModelView getThisPersonne(){
        ModelView valiny = new ModelView();
        HashMap<String, Object> mDatas = new HashMap();
        
        mDatas.put("nom", this.getNomPersonne());
        mDatas.put("prenom", this.getPrenomPersonne());
        mDatas.put("age", this.getAgePersonne());

        valiny.setDatas(mDatas);
        
        valiny.setJspToGo("listePersonne.jsp");
        return valiny;
    }
}
